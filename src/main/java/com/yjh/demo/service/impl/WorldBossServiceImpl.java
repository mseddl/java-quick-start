package com.yjh.demo.service.impl;

import com.yjh.demo.entity.WorldBoss;
import com.yjh.demo.mapper.WorldBossMapper;
import com.yjh.demo.service.WorldBossService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 世界boss 服务实现类
 * </p>
 *
 * @author yujiahui
 * @since 2018-12-29
 */
@Service
public class WorldBossServiceImpl extends ServiceImpl<WorldBossMapper, WorldBoss> implements WorldBossService {

}
