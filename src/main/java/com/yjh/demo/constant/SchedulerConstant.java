package com.yjh.demo.constant;

/**
 * @Auther: yujiahui
 * @Date: 2018/12/29 17:05
 * @Description:
 */
public class SchedulerConstant {
    /**
     * <p>Example patterns:
     * <ul>
     * <li>"0 0 * * * *" = the top of every hour of every day.</li>
     * <li>"* 10 * * * * *" = every ten seconds.</li>
     * <li>"0 0 8-10 * * *" = 8, 9 and 10 o'clock of every day.</li>
     * <li>"0 0 6,19 * * *" = 6:00 AM and 7:00 PM every day.</li>
     * <li>"0 0/30 8-10 * * *" = 8:00, 8:30, 9:00, 9:30, 10:00 and 10:30 every day.</li>
     * <li>"0 0 9-17 * * MON-FRI" = on the hour nine-to-five weekdays</li>
     * <li>"0 0 0 25 12 ?" = every Christmas Day at midnight</li>
     * </ul>
     */
    /**
     * 排行榜结算时间配置:每天20点
     * */
    public static final String RANKING_LIST_MAIL = "0 0 20 * * *";
}
