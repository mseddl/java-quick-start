package com.yjh.demo.scheduler;

/**
 * @author yujiahui
 * @Description: $计划任务的通用接口
 * @date 2018/8/10 13:47
 */
public interface Scheduler {
    /**
     * 执行计划任务
     */
    void doSchedulerWork();
}
