package com.yjh.demo.common;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回数据
 * @Auther: yujiahui
 * @Date: 2018/12/29 16:20
 * @Description:
 */
public class Response extends HashMap<String, Object> {

    private static final long serialVersionUID = 1L;

    public Response() {
        put("code", 0);
        put("msg", "success");
    }

    public static Response error() {
        return error(500, "未知异常，请联系管理员");
    }

    public static Response error(String msg) {
        return error(500, msg);
    }

    public static Response error(int code, String msg) {
        Response json = new Response();
        json.put("code", code);
        json.put("msg", msg);
        return json;
    }
    public static Response ok(Object msg) {
        Response json = new Response();
        json.put("msg", msg.toString());
        return json;
    }

    public static Response ok(Map<String, Object> map) {
        Response json = new Response();
        json.putAll(map);
        return json;
    }

    public static Response ok() {
        return new Response();
    }

    @Override
    public Response put(String key, Object value) {
        super.put(key, value);
        return this;
    }
    
}
