package com.yjh.demo.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import java.io.Serializable;

/**
 * <p>
 * 世界boss的用户
 * </p>
 *
 * @author yujiahui
 * @since 2018-12-29
 */
public class WorldBossUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "wbu_id", type = IdType.AUTO)
    private Integer wbuId;
    private String userId;
    /**
     * boss的id
     */
    private Integer wbId;
    /**
     * 击杀的血量
     */
    private Long wbuHp;
    /**
     * 开始的时间
     */
    private Integer wbuStartTime;
    /**
     * 复活时间
     */
    private Integer wbuRelivedTime;
    /**
     * 离开时间
     */
    private Integer wbuLeaveTime;
    /**
     * 死亡次数
     */
    private Integer wuDieNum;
    /**
     * 是否领取了奖励
     */
    private Integer wuAwarded;


    public Integer getWbuId() {
        return wbuId;
    }

    public void setWbuId(Integer wbuId) {
        this.wbuId = wbuId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getWbId() {
        return wbId;
    }

    public void setWbId(Integer wbId) {
        this.wbId = wbId;
    }

    public Long getWbuHp() {
        return wbuHp;
    }

    public void setWbuHp(Long wbuHp) {
        this.wbuHp = wbuHp;
    }

    public Integer getWbuStartTime() {
        return wbuStartTime;
    }

    public void setWbuStartTime(Integer wbuStartTime) {
        this.wbuStartTime = wbuStartTime;
    }

    public Integer getWbuRelivedTime() {
        return wbuRelivedTime;
    }

    public void setWbuRelivedTime(Integer wbuRelivedTime) {
        this.wbuRelivedTime = wbuRelivedTime;
    }

    public Integer getWbuLeaveTime() {
        return wbuLeaveTime;
    }

    public void setWbuLeaveTime(Integer wbuLeaveTime) {
        this.wbuLeaveTime = wbuLeaveTime;
    }

    public Integer getWuDieNum() {
        return wuDieNum;
    }

    public void setWuDieNum(Integer wuDieNum) {
        this.wuDieNum = wuDieNum;
    }

    public Integer getWuAwarded() {
        return wuAwarded;
    }

    public void setWuAwarded(Integer wuAwarded) {
        this.wuAwarded = wuAwarded;
    }

    @Override
    public String toString() {
        return "WorldBossUser{" +
        ", wbuId=" + wbuId +
        ", userId=" + userId +
        ", wbId=" + wbId +
        ", wbuHp=" + wbuHp +
        ", wbuStartTime=" + wbuStartTime +
        ", wbuRelivedTime=" + wbuRelivedTime +
        ", wbuLeaveTime=" + wbuLeaveTime +
        ", wuDieNum=" + wuDieNum +
        ", wuAwarded=" + wuAwarded +
        "}";
    }
}
