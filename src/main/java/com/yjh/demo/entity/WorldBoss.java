package com.yjh.demo.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * <p>
 * 世界boss
 * </p>
 *
 * @author yujiahui
 * @since 2018-12-29
 */
@TableName("world_boss")
public class WorldBoss implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("wb_id")
    private Integer wbId;
    /**
     * 当前状态，是等待倒计时，还是正在打斗
     */
    private Integer wbStatus;
    /**
     * 召唤的时间
     */
    private Integer wbCallTime;
    /**
     * 剩余血量
     */
    private Long wbHp;
    /**
     * 是否被击杀了 
     */
    private Integer wbKilled;
    /**
     * 击杀人的id
     */
    private String wbKillId;
    /**
     * 本次是否发奖励了
     */
    private Integer wbAwded;
    /**
     * 下次可召唤的时间
     */
    private Integer wbNextTime;
    /**
     * 召唤的类型
     */
    private Integer wbType;
    /**
     * 参与人数
     */
    private Integer wbPeopleNum;
    /**
     * 世界boss的拥有者id
     */
    private String wbOwnUid;
    /**
     * 归属者的Hp
     */
    private Integer wbOwnHp;
    /**
     * 世界boss满血血量
     */
    private Long bossHpFull;
    /**
     * 世界boss是否开过护盾，0没开过，1开过
     */
    private Integer bossShield;
    /**
     * 世界boss护盾开启时间
     */
    private Integer bossShieldTime;


    public Integer getWbId() {
        return wbId;
    }

    public void setWbId(Integer wbId) {
        this.wbId = wbId;
    }

    public Integer getWbStatus() {
        return wbStatus;
    }

    public void setWbStatus(Integer wbStatus) {
        this.wbStatus = wbStatus;
    }

    public Integer getWbCallTime() {
        return wbCallTime;
    }

    public void setWbCallTime(Integer wbCallTime) {
        this.wbCallTime = wbCallTime;
    }

    public Long getWbHp() {
        return wbHp;
    }

    public void setWbHp(Long wbHp) {
        this.wbHp = wbHp;
    }

    public Integer getWbKilled() {
        return wbKilled;
    }

    public void setWbKilled(Integer wbKilled) {
        this.wbKilled = wbKilled;
    }

    public String getWbKillId() {
        return wbKillId;
    }

    public void setWbKillId(String wbKillId) {
        this.wbKillId = wbKillId;
    }

    public Integer getWbAwded() {
        return wbAwded;
    }

    public void setWbAwded(Integer wbAwded) {
        this.wbAwded = wbAwded;
    }

    public Integer getWbNextTime() {
        return wbNextTime;
    }

    public void setWbNextTime(Integer wbNextTime) {
        this.wbNextTime = wbNextTime;
    }

    public Integer getWbType() {
        return wbType;
    }

    public void setWbType(Integer wbType) {
        this.wbType = wbType;
    }

    public Integer getWbPeopleNum() {
        return wbPeopleNum;
    }

    public void setWbPeopleNum(Integer wbPeopleNum) {
        this.wbPeopleNum = wbPeopleNum;
    }

    public String getWbOwnUid() {
        return wbOwnUid;
    }

    public void setWbOwnUid(String wbOwnUid) {
        this.wbOwnUid = wbOwnUid;
    }

    public Integer getWbOwnHp() {
        return wbOwnHp;
    }

    public void setWbOwnHp(Integer wbOwnHp) {
        this.wbOwnHp = wbOwnHp;
    }

    public Long getBossHpFull() {
        return bossHpFull;
    }

    public void setBossHpFull(Long bossHpFull) {
        this.bossHpFull = bossHpFull;
    }

    public Integer getBossShield() {
        return bossShield;
    }

    public void setBossShield(Integer bossShield) {
        this.bossShield = bossShield;
    }

    public Integer getBossShieldTime() {
        return bossShieldTime;
    }

    public void setBossShieldTime(Integer bossShieldTime) {
        this.bossShieldTime = bossShieldTime;
    }

    @Override
    public String toString() {
        return "WorldBoss{" +
        ", wbId=" + wbId +
        ", wbStatus=" + wbStatus +
        ", wbCallTime=" + wbCallTime +
        ", wbHp=" + wbHp +
        ", wbKilled=" + wbKilled +
        ", wbKillId=" + wbKillId +
        ", wbAwded=" + wbAwded +
        ", wbNextTime=" + wbNextTime +
        ", wbType=" + wbType +
        ", wbPeopleNum=" + wbPeopleNum +
        ", wbOwnUid=" + wbOwnUid +
        ", wbOwnHp=" + wbOwnHp +
        ", bossHpFull=" + bossHpFull +
        ", bossShield=" + bossShield +
        ", bossShieldTime=" + bossShieldTime +
        "}";
    }
}
