package com.yjh.demo.service;

import com.yjh.demo.entity.WorldBoss;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 世界boss 服务类
 * </p>
 *
 * @author yujiahui
 * @since 2018-12-29
 */
public interface WorldBossService extends IService<WorldBoss> {

}
