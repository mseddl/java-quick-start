import com.yjh.demo.WebApplication;
import com.yjh.demo.entity.WorldBoss;
import com.yjh.demo.service.WorldBossService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApplication.class)
public class testFunc{

    @Autowired
    private WorldBossService worldBossService;

    @Test
    public void testSqlSession(){
        int index = 1;
        WorldBoss worldBoss = worldBossService.selectById(index);
        System.out.println(worldBoss.toString());
    }

}