package com.yjh.demo.controller;


import com.alibaba.druid.support.json.JSONUtils;
import com.yjh.demo.access.AccessLimit;
import com.yjh.demo.common.Response;
import com.yjh.demo.entity.WorldBoss;
import com.yjh.demo.service.WorldBossService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * <p>
 * 世界boss 前端控制器
 * </p>
 *
 * @author yujiahui
 * @since 2018-12-29
 */
@Controller
@RequestMapping("/worldboss")
public class WorldBossController {
    @Autowired
    private WorldBossService worldBossService;

    @AccessLimit
    @GetMapping("hello")
    @ResponseBody
    public Response hello(){
        int a = 1;
        WorldBoss worldBoss = worldBossService.selectById(a);
        return Response.ok((worldBoss.toString()));
    }

}

