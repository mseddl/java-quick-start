package com.yjh.demo.scheduler;

import com.yjh.demo.constant.SchedulerConstant;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * @author yujiahui
 * @Description: ${todo}
 * @date 2018/8/10 13:48
 */
@EnableScheduling
@Configuration
public class RankingListScheduler implements Scheduler {

    @Scheduled(cron = SchedulerConstant.RANKING_LIST_MAIL)
    @Override
    public void doSchedulerWork() {
        System.out.println("发邮件啦!");
    }
}
