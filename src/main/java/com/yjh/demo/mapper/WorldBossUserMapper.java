package com.yjh.demo.mapper;

import com.yjh.demo.entity.WorldBossUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 世界boss的用户 Mapper 接口
 * </p>
 *
 * @author yujiahui
 * @since 2018-12-29
 */
public interface WorldBossUserMapper extends BaseMapper<WorldBossUser> {

}
