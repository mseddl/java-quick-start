package com.yjh.demo.service.impl;

import com.yjh.demo.entity.WorldBossUser;
import com.yjh.demo.mapper.WorldBossUserMapper;
import com.yjh.demo.service.WorldBossUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 世界boss的用户 服务实现类
 * </p>
 *
 * @author yujiahui
 * @since 2018-12-29
 */
@Service
public class WorldBossUserServiceImpl extends ServiceImpl<WorldBossUserMapper, WorldBossUser> implements WorldBossUserService {

}
