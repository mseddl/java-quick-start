package com.yjh.demo.service;

import com.yjh.demo.entity.WorldBossUser;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 世界boss的用户 服务类
 * </p>
 *
 * @author yujiahui
 * @since 2018-12-29
 */
public interface WorldBossUserService extends IService<WorldBossUser> {

}
