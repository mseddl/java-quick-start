package com.yjh.demo.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 世界boss的用户 前端控制器
 * </p>
 *
 * @author yujiahui
 * @since 2018-12-29
 */
@Controller
@RequestMapping("/worldBossUser")
public class WorldBossUserController {

}

