package com.yjh.demo.mapper;

import com.yjh.demo.entity.WorldBoss;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 世界boss Mapper 接口
 * </p>
 *
 * @author yujiahui
 * @since 2018-12-29
 */
public interface WorldBossMapper extends BaseMapper<WorldBoss> {

}
